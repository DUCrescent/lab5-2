## Name
简约绘图程序II
## Description
这个简约绘图程序允许用户在画布上绘制点、直线、圆形和矩形。
## Badges

## Visuals

## Installation
Qt 5和编译器
## Usage
新建图形文件：点击菜单栏中的 "New" 按钮来开始一个新的绘图。
打开图形文件：点击菜单栏中的 "Open" 按钮来打开一个已有的绘图文件。
保存图形文件：点击菜单栏中的 "Save" 按钮来保存当前绘图文件。
另存图形文件：点击菜单栏中的 "Save As" 按钮来将当前绘图文件另存为一个新文件。
退出程序：点击菜单栏中的 "Exit" 按钮来退出程序。

## Support


## Roadmap

## Contributing

## Authors and acknowledgment
杜邦洲 dubangzhou@163.com DUCrescent

## License


## Project status

